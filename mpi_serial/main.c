#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>

int width, height, nIter;
float outOfBoundVal = 1.0e-6;
float **S, **V, **T, **M, **D, **L;
float sum = 0.0f, max = FLT_MIN, avg;

void initialize();
void iterate();
void update();
void stats();
void createMatrix();
void freeMatrix();
void printMatrix(float**);

int main(int argc, char* argv[])
{
	if (argc != 4) {
		printf("Arguments missing.\narg1=width arg2=height arg3=nIter\n");
		return 1;
	}

	width = atoi(argv[1]);
	height = atoi(argv[2]);
	nIter = atoi(argv[3]);

	createMatrix();
	initialize();
	iterate();
	stats();
	freeMatrix();

	printf("MAX=%f AVG=%f SUM=%f\n", max, avg, sum);

	return 0;
}

void initialize()
{
	int i, j;

	// initialize V,S,T
		for (i = 0; i < height; i++)
			for (j = 0; j < width; j++)
				V[i][j] = S[i][j] = T[i][j] = 1.0e-6;

	// initialize D
	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++) {
			if (i == 0 || i == height - 1 ||
				j == 0 || j == width - 1)
				D[i][j] = 0.6f;
			else if (i == 1 || i == height - 2 ||
				j == 1 || j == width - 2)
				D[i][j] = 0.7f;
			else if (i == 2 || i == height - 3 ||
				j == 2 || j == width - 3)
				D[i][j] = 0.8f;
			else if (i == 3 || i == height - 4 ||
				j == 3 || j == width - 4)
				D[i][j] = 0.9f;
			else
				D[i][j] = 1.0f;
		}

	// initialize M,L
	for (i = 0; i < height / 2; i++)
		for (j = 0; j < width; j++)
			M[i][j] = L[i][j] = 0.125f;
	for (i = height / 2; i < height; i++)
		for (j = 0; j < width; j++) {
			M[i][j] = 0.3f;
			L[i][j] = 0.4f;
		}
}

void iterate()
{
	int i;
	int pulse_x, pulse_y, pulse_counter;
	float t;
	pulse_x = width / 3;
	pulse_y = height / 4;
	pulse_counter = 10;

	for (i = 0; i < nIter; i++) {
		// initial impulse
		if (i < 10) {
			t = (pulse_counter - i) * 0.05f;
			V[pulse_y][pulse_x] += 64 * sqrt(M[pulse_y][pulse_x]) * exp(-t * t);
		}

		/*printf("it #%d\n", i);
		printMatrix(V);*/

		update();
	}
}

void update()
{
	//S[i, j, it + 1] = S[i, j, it] + M[i, j] * (V[i, j + 1, it] - V[i, j, it])	
	//T[i, j, it + 1] = T[i, j, it] + M[i, j] * (V[i + 1, j, it] - V[i, j, it])	
	//V[i, j, it + 1] = D[i, j] * (V[i, j, it] + L[i, j] * (S[i, j, it + 1] - S[i, j - 1, it + 1] + T[i, j, it + 1] - T[i - 1, j, it + 1]))

	int i, j;
	float vVal, sVal, tVal;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			vVal = (i + 1 < height) ? V[i + 1][j] : outOfBoundVal;
			T[i][j] = T[i][j] + M[i][j] * (vVal - V[i][j]);

			vVal = (j + 1 < width) ? V[i][j + 1] : outOfBoundVal;
			S[i][j] = S[i][j] + M[i][j] * (vVal -V[i][j]);

			sVal = (j >= 1) ? S[i][j - 1] : outOfBoundVal;
			tVal = (i >= 1) ? T[i - 1][j] : outOfBoundVal;

			V[i][j] = D[i][j] * (V[i][j]+ L[i][j] * (S[i][j] - sVal + T[i][j] - tVal));
		}
	}
}

void stats()
{
	int i, j;

	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++) {
			sum += V[i][j];

			if (V[i][j] > max)
				max = V[i][j];
		}

	avg = sum / (width * height);
}

void createMatrix()
{
	int i;

	S = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		S[i] = (float*)malloc(width * sizeof(float));
	V = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		V[i] = (float*)malloc(width * sizeof(float));
	T = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		T[i] = (float*)malloc(width * sizeof(float));

	M = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		M[i] = (float*)malloc(width * sizeof(float));
	D = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		D[i] = (float*)malloc(width * sizeof(float));
	L = (float**)malloc(height * sizeof(float*));
	for (i = 0; i < height; i++)
		L[i] = (float*)malloc(width * sizeof(float));
}

void freeMatrix()
{
	int i;

	for (i = 0; i < height; i++) {
		free(S[i]);
		free(V[i]);
		free(T[i]);
		free(M[i]);
		free(D[i]);
		free(L[i]);
	}

	free(S);
	free(V);
	free(T);
	free(M);
	free(D);
	free(L);
}

void printMatrix(float** matrix)
{
	int i, j;

	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++)
			printf("%f ", matrix[i][j]);
		printf("\n");
	}
	printf("\n");
}